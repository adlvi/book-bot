<?php
class DB{

    public $pdo;
public function __construct($args){
  
    $match=explode(";",$args);
    $DB=$match[0];
    $HOST=$match[1];
    $USER=$match[2];
    $PASS=$match[3];
    $this->pdo=new PDO("mysql:dbname=$DB;host=$HOST",$USER,$PASS,[PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"]);
   // $this->pdo->exec("set names utf8");
}
public function pdo(){
    return $this->pdo;
}

public function insert($table,$fields,$retFD=null){
    $col = "insert into $table (`".implode("` , `",array_keys($fields))."`)";
    $val = " values(";		
$rep=str_repeat("?,",count($fields));
    $val .= substr($rep,0,(strlen($rep))-1).");";	
    $pre=$this->pdo->prepare($col." ".$val);
    $pre->execute(array_values($fields));
    
    return is_null($retFD)?$this->pdo->lastInsertId():$this->pdo->lastInsertId($retFD);
    
}

public function update($table,$fields,$condition) {
    
    $sql = "update $table set `".implode("`=?,`",array_keys($fields))."`=?";

    $sql .= " where ".$condition.";";	

  $pre=$this->pdo->prepare($sql);
  
  return  $pre->execute(array_values($fields));
    
    
}

public function select($table,$fields,$condition){   
$query="SELECT ".implode(",",array_values($fields))." FROM $table WHERE $condition;";
$pre=$this->pdo->prepare($query);
$pre->execute();

$row=$pre->fetchAll(PDO::FETCH_ASSOC);

   return $row;	
}

public function delete($table,$condition){   
$query="DELETE FROM $table WHERE $condition;";

$pre=$this->pdo->prepare($query);
return $pre->execute();

}
public function query($query){   


return $this->pdo->query($query);
//return $pre->execute();

}
public function replace($table,$fields,$retFD=null){
 
    $col = "replace into $table (`".implode("` , `",array_keys($fields))."`)";
    $val = " values(";		
$rep=str_repeat("?,",count($fields));
    $val .= substr($rep,0,(strlen($rep))-1).");";	
    $pre=$this->pdo->prepare($col." ".$val);
  
    $pre->execute(array_values($fields));
    $pre->debugDumpParams();

    return is_null($retFD)?$this->pdo->lastInsertId():$this->pdo->lastInsertId($retFD);
    
}



    
    
    
}