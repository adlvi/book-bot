<?php

class User{
    public static function setStep($uid=null,$step=null,$append=false){
        $uid=$uid??fid;
        global $user;
      
        global $DB;
        $step1=!is_null($step)&&$append?$user['step']."/$step":$step; 
        $user['step']=$step1;
       return $DB->update('user',['step'=>$step1],"UID=".$uid);
    }
    public static function resetStep($fid=null,$sure=false){
        $fid=$fid??fid;
        global $user,$DB,$bot,$sureResetMenu;
        if(self::haveStep($fid) && self::isStep("addItem",$fid) && !$sure){
            $bot->msg("مراحل افزودن ایتم جدید به پایان نرسیده است اگر مایل به ادامه هستید اطلاعات ثبت شده در این ایتم حذف میشود؟
آیا مایل به حذف هستید ؟",msgid,"MarkDown",$sureResetMenu);
           return false; 
        }
        else{
            self::setStep($fid,null);
            return true;
        }
    } 
    public static function haveStep($fid=null)
    {
    
        global $user,$DB;
        if(is_null($fid)){
            return !is_null($user['step'])&&!empty($user['step']);
        }else{
            $nu=$DB->select("user",['step'],"UID=".$fid);
            return !empty($nu)&&(!is_null($nu[0]['step'])&&!empty($nu[0]['step']));
        }
    }
    public static function isStep($step,$fid=null){
        $u=[];
        global $user,$DB;
        if(is_null($fid))
        $u=$user;
        else{
          $nu=$DB->select("user",['step'],"UID=".$fid); 
        $u= empty($nu)?["step"=>null]:$nu[0];  
        }
      
        return preg_match("/$step/ius",$u['step']);
    }
}

class Item{
    
    public static function addItem($forSale=1,$fid=null){
    
        $fid=$fid??fid;
        global $DB;
        $ID= $DB->insert("item",['forSale'=>$forSale,'UID'=>$fid],"ID");
        User::setStep($fid,"addItem/$ID");
        return $ID;
    }
    public static function selectItem($ID,array $what=['*']){
        global $DB;
        
        return !empty($item=$DB->select("item",$what,"ID=".$ID))?$item[0]:[];
    }
   public static function getItemIDByUserStep(){
        global $user;
        if(User::haveStep() && (User::isStep('addItem') || User::isStep("editItem"))){
            return explode("/",$user['step'])[1];
        }
        return false;
    }
    public static function createCurrentPath($fid){
        $usr=[];
        if($fid==fid){
            global $user;
            $usr=$user;
        }else{
            global $DB;
            $sel=$DB->select("user",['step'],"UID=$fid");
            if(count($sel)==0){
                return ["ok"=>false,'result'=>[]];
            }
            else{
                $usr=$sel[0];
            }}
            if(is_null($usr['step']))
             return ["ok"=>false,'result'=>""];
             else{
                 
                 $ret="افزودن ایتم";
                 $step=explode("/",$usr['step']);
                $it= self::selectItem(self::getItemIDByUserStep());
                $typ=$it['type']=="book"?"کتاب":"جزوه";
                 $other=[
                     ['title'=>"$typ",'name'=>"setType"],
                         ['title'=>"نام","name"=>"addName"],
                            ["title"=>"عکس","name"=>"addImage"],
                                ["title"=>"توضیحات","name"=>"addAbout"]
                 ];
                 
                 $cur=$step[2];
                  if(count($step)==1)
                        return ['ok'=>true,'result'=>$ret.":"];
                 foreach($other as $k=>$v){
                    
                     if(!preg_match("/{$v['name']}/ius",$cur))
                        $ret.=" > {$v['title']}";
                     else{
                      $ret.=" > *{$v['title']}*:";    
                     break;
                        }
                 }
                 return ['ok'=>true,'result'=>$ret];
                 
             }
            
        
        
        
    }
    public static function sendItemToVerify($ID){
        global $bot,$DB;
        $item=self::selectItem($ID);
        $cap=self::createCaption($item);
        $keys=['inline_keyboard'=>[
            [
                ['text'=>"تایید",'callback_data'=>"/verify $ID"],['text'=>"رد",'callback_data'=>"/disapproval $ID"]
                ]
            ]];
       return $bot->sendPhoto(['chat_id'=>verifyChatID,"photo"=>$item['image'],'caption'=>$cap,'reply_markup'=>json_encode($keys)]);
    }
    public static function sendSearchedItem($ID,$fid=null){
        $fid=$fid??fid;
        global $bot,$DB;
        $key=[];
        $item=self::selectItem($ID);
        
        if($item['forSale']==="1")
        $key['inline_keyboard']=[[['text'=>"خرید",'callback_data'=>"/buy {$item['ID']}"]]];
        else{
            $br=$DB->select("borrow",['IID',"time",'duration']," IID=$ID and time<=".time()." and time+(duration*86400)>=".time()." limit 1");
            if(count($br)==0)
                 $key['inline_keyboard']=[[['text'=>"امانت",'callback_data'=>"/borrow {$item['ID']}"]]];
            else
                 $key['inline_keyboard']=[[['text'=>"رزرو",'callback_data'=>"/reserve {$item['ID']}"]]];
        }
        $cap=self::createCaption($item);
        
       return $bot->sendPhoto(['chat_id'=>$fid,"photo"=>$item['image'],'caption'=>$cap,'reply_markup'=>json_encode($key)]);
    }
        public static function sendUserItem($ID,$fid=null){
        global $bot,$DB;
        $key=[];
        $fid=$fid??fid;
        $cap="";
        $item=self::selectItem($ID);
        $k=null;
        if($item['forSale']==="1"){
      $sell = $DB->select('sell',['*'],"IID=$ID and UID=$fid");
            $cap.="*برای فروش* : ".(count($sell)!=0?"فروخته شده به ".$bot->mention($sell[0]['UID'],$sell[0]['UID'])['result']['mention']:"`فروخته نشده`").PHP_EOL;
          
        }
        else{
             $br=$DB->select("borrow",['IID',"time",'duration','UID']," IID=$ID and time<=".time()." and time+(duration*86400)>=".time()." limit 1");
             $date=count($br)>0?jdate("Y/m/d h:i:s" ,$br[0]['time'], null ,"Asia/Tehran" ,"en" ):"";
             $day=count($br)>0?$br[0]['duration']:0;
             
             if(count($br)>0)
             $k=json_encode(['inline_keyboard'=>[[['text'=>"بازگشت پیش از موعد",'callback_data'=>"/backBeforeDue ".$br[0]['UID']." $ID"]]]]);
             
           //  $bot->msg($k);
            $cap.="*برای امانت* : ".(count($br)!=0?"`امانت داده شده به` ".$bot->mention($br[0]['UID'],$br[0]['UID'])['result']['mention']."` در تاریخ $date به مدت $day روز` "/**/:"`امانت داده نشده`").PHP_EOL;;
           
           
                 
        }
        $cap=self::createCaption($item).PHP_EOL.$cap;
        
       return $bot->sendPhoto(['chat_id'=>$fid,"photo"=>$item['image'],'caption'=>$cap,'reply_markup'=>$k]);
    }
    
     public static function sendUserRecivedItem($ID,$fid=null){
        global $bot,$DB;
        $fid=$fid??fid;
        $item=self::selectItem($ID);
        $cap=self::createCaption($item).PHP_EOL;
        
            if($item['forSale']==="1"){
      $sell = $DB->select('sell',['time'],"IID=$ID and UID=$fid");
       $date=jdate("Y/m/d h:i:s" ,$sell[0]['time'], null ,"Asia/Tehran" ,"en" );
             
            $cap.=PHP_EOL."*خریده شده در تاریخ* : $date ".PHP_EOL;
          
        }
        else{
             $br=$DB->select("borrow",['IID',"time",'duration','UID']," IID=$ID  limit 1");
             $date=jdate("Y/m/d h:i:s" ,$br[0]['time'], null ,"Asia/Tehran" ,"en" );
             $day=$br[0]['duration'];
             
             
           //  $bot->msg($k);
            $cap.=PHP_EOL."* امانت گرفته شده در تاریخ $date به مدت $day روز* ";
           
           
                 
        }
        
        
       return $bot->sendPhoto(['chat_id'=>$fid,"photo"=>$item['image'],'caption'=>$cap]);
    }
    
    public static function showItem($ID,$fid=null,$showDate=true){
        global $bot;
        $fid=is_null($fid)?fid:$fid;
        $item=self::selectItem($ID);
        
        $cap=self::createCaption($item,$showDate);
        
       return $bot->sendPhoto(['chat_id'=>$fid,"photo"=>$item['image'],'caption'=>$cap]);
    }
    private static function createCaption($item,$showDate=true){
        global $DB,$bot;
        $ID=$item['ID'];
        $cap="";
        $cap.="نام: ".Help2::scapeChars($item['name']).PHP_EOL;
        $cap.="نوع :".($item['type']==="book"?"*کتاب*":"*جزوه*").PHP_EOL;
        $special=0;
        //set special field to price or max time of back...
        if($item['forSale']==="1"){
            $special=$DB->select("forSale",['price'],"ID=$ID")[0]['price'];
        }else{
            $special=$DB->select("forBorrow",['maxTimeOfBack'],"ID=$ID")[0]['maxTimeOfBack'];
        }
        //
        $cap.=($item['forSale']==="1"?"قیمت :"."*".number_format($special)."*":"حداکثر زمان بازگشت: *$special*").PHP_EOL;
        $cap.="توسط: [".Help2::scapeChars($bot->getCM($item['UID'],$item['UID'])['result']['user']['first_name'])."](tg://user?id=".$item['UID'].")".PHP_EOL;
        $cap.=$showDate?"زمان ثبت: *".jdate("Y/m/d h:i:s" ,$item['addTime'], null ,"Asia/Tehran" ,"en" )."*".str_repeat(PHP_EOL,2):""; //exaple: 1398/8/22 22:45:30
        $cap.="توضیحات: *".PHP_EOL.Help2::scapeChars($item['about'])."*";
        return $cap;
    }
    public static function searchItemByQuery($text){
        global $DB,$bot;
        preg_match_all("/[\w\d\s".Help2::$faWord.implode("",Help2::$faNum)."]+/ius",$text,$mtc);
        $text=implode(" ",$mtc[0]);
        $text=preg_replace("/(\s+)/ius","%",trim($text,' '));
       // $bot->msg(Help2::scapeChars($text));
        $spl=preg_split("/\%/ius",$text,-1, PREG_SPLIT_NO_EMPTY);
        $con=" '%".implode("%' or name like '%",$spl)."%' ";
        $con1=" '%".implode("%' or about like '%",$spl)."%' ";
      
        $pre=$DB->pdo()->prepare("select * from item  where active=1 and item.ID not in (select IID from sell where 1 ) and  (name like '%$text%' or name like $con or about like '%$text%' or about like $con1)");
        $pre->execute();
        $res=$pre->fetchAll(PDO::FETCH_ASSOC);
       // Help2::save($q,0,"q.txt");
        return $res;
        
    }
    public static function selectUserItems($fid){
        global $DB;
      
    
        $pre=$DB->pdo()->prepare("select * from item where UID=?");
        $pre->execute([$fid]);
        $res=$pre->fetchAll(PDO::FETCH_ASSOC);
     //  Help2::save($res);
        return $res;
        
    }
        public static function selectUserRecivedItems($fid){
        global $DB;
      
    
        $pre=$DB->pdo()->prepare("select * from item where ID in ( select IID from borrow where UID=? union select IID from sell where UID=?)");
        $pre->execute([$fid,$fid]);
        $res=$pre->fetchAll(PDO::FETCH_ASSOC);
      // Help2::save($res);
        return $res;
        
    }
    public static function isSold($ID){
        global $DB;
        return count($DB->select("sell",["IID"],"IID=$ID limit 1"))==1;
    }
     public static function isBorrow($ID){
        global $DB;
         return count($DB->select("borrow",["IID"],"IID=$ID and time<=".time()." and time+(duration*86400)>=".time()." limit 1"))==1;
        
    }
    public static function createItemKeyboard($items = [],$column=1)
    {
        $keys = [];
        foreach($items as $item)
        {
            $keys[]=["text"=>($item['type']=== "book"?"📚":"📑").($item['forSale']==1?"💵":"🔂")." ".$item['name'],"callback_data"=>"/show {$item['ID']}"];
        }
		return array_chunk($keys,$column);
    }
       public static function createUserItemKeyboard($items = [],$column=1)
    {
        $keys = [];
        foreach($items as $item)
        {
            $keys[]=["text"=>($item['type']=== "book"?"📚":"📑").($item['forSale']==1?"💵":"🔂")." ".$item['name'],"callback_data"=>"/myshow {$item['ID']}"];
        }
		return array_chunk($keys,$column);
    }
    
       public static function createUserRecivedItemKeyboard($items = [],$column=1)
    {
        $keys = [];
        foreach($items as $item)
        {
            $keys[]=["text"=>($item['type']=== "book"?"📚":"📑").($item['forSale']==1?"💵":"🔂")." ".$item['name'],"callback_data"=>"/myshowrecived {$item['ID']}"];
        }
     file_put_contents("res3.json",json_encode($key,384));
		return array_chunk($keys,$column);
    }
    
    public static function requestToBuy($ID,$fid=null){
        $fid=$fid??fid;
        $item=self::selectItem($ID);
        global $bot,$DB;
        $menSelf=$bot->mention();
        $menSeller=$bot->mention($item['UID'],$item['UID']);
        if(!$menSeller['ok']){
        $bot->msg("به نظر میرسد اکانت فروشنده در دسترس نمیباشد!! لطفا مجددا جستجو نمایید");
            return ['ok'=>false];
        }
        if(self::isSold($ID)){
        $bot->ACQ(cbid,"این ایتم به فروش رسیده است!!!\nلطفا مجددا جستجو کنید");
            return ['ok'=>false];
        }
        $res=  $bot->editMessageReplyMarkup(['chat_id'=>$fid,"message_id"=>msgid]);
         $bot->msg("درخواست شما برای خرید به ".$menSeller['result']['mention']." ارسال شد.");
        $res=self::showItem($ID,$item['UID']);
        
        $key=['inline_keyboard'=>[[['text'=>"تایید","callback_data"=>"/acceptBuyRequest ".$item['ID']." ".$fid],['text'=>"رد","callback_data"=>"/disapprovalBuyRequest {$item['ID']} ".$fid]]]];
    return $bot->sm($item['UID'],"کاربر ".$menSelf['result']['mention']." قصد خرید ایتم فوق را دارد!!\n*لطفا پس از ارتباط و ایجاد توافق با وی کلید تایید را فشار دهید و در غیر این صورت درخواست را رد نمایید*",$res['result']['message_id'],"MarkDown",$key);
    }
     public static function requestToBorrow($ID,$fid=null){
         $fid=$fid??fid;
        $item=self::selectItem($ID);
        global $bot,$DB;
        $menSelf=$bot->mention();
        $menSeller=$bot->mention($item['UID'],$item['UID']);
        if(!$menSeller['ok']){
        $bot->msg("به نظر میرسد اکانت فروشنده در دسترس نمیباشد!! لطفا مجددا جستجو نمایید");
            return ['ok'=>false];
        }
        if(self::isBorrow($ID)){
        $bot->ACQ(cbid,"این ایتم در دست امانت است \nدر صورت تمایل میتوانید درخواست خود را برای رزرو ایتم ثبت کنید.");
        return self::requestToReserve($ID,$fid);
            
        }
        $max=item::getMaxTimeOfBack($ID);
        $res=  $bot->editMessageReplyMarkup(['chat_id'=>$fid,"message_id"=>msgid]);
        $bot->sm($fid,"برای چند روز قصد امانت  ایتم فوق را دارید؟\n*حد اکثر عدد مجاز* `$max` *روز میباشد.*",-1);
        User::setStep($fid,"getTimeOfBack/$ID");
        return ['ok'=>true];
            }
    public static function FinalRequestToBorrow($ID,$day,$fid=null){
        $fid=$fid??fid;
           $item=self::selectItem($ID);
        global $bot,$DB;
        $menSelf=$bot->mention();
        $menSeller=$bot->mention($item['UID'],$item['UID']);
        if(!$menSeller['ok']){
        $bot->msg("به نظر میرسد اکانت فروشنده در دسترس نمیباشد!! لطفا مجددا جستجو نمایید");
            return ['ok'=>false];
        }
        if(self::isBorrow($ID)){
        $bot->ACQ(cbid,"این ایتم در دست امانت است \nدر صورت تمایل میتوانید درخواست خود را برای رزرو ایتم ثبت کنید.");
        return self::requestToReserve($ID,$fid);
            
        }
        $bot->msg("درخواست شما برای امانت به ".$menSeller['result']['mention']." ارسال شد.");
        $res=self::showItem($ID,$item['UID'],false);
        
        $key=['inline_keyboard'=>[[['text'=>"تایید","callback_data"=>"/acceptBorrowRequest ".$item['ID']." ".$day." ".$fid],['text'=>"رد","callback_data"=>"/disapprovalBorrowRequest {$item['ID']} ".$day." ".$fid]]]];
      return  $bot->sm($item['UID'],"کاربر ".$menSelf['result']['mention']." قصد امانت ایتم فوق را دارد!!\n*لطفا پس از ارتباط و ایجاد توافق با وی کلید تایید را فشار دهید و در غیر این صورت درخواست را رد نمایید*",$res['result']['message_id'],"MarkDown",$key);

    } 
    public static function getMaxTimeOfBack($ID){
        global $DB;
        return $DB->select('forBorrow',['maxTimeOfBack'],"ID=$ID")[0]['maxTimeOfBack']??-1;
    }
     public static function requestToReserve($ID,$fid=null){
         $fid=$fid??fid;
        $item=self::selectItem($ID);
        global $bot,$DB;
        $menSelf=$bot->mention();
        $menSeller=$bot->mention($item['UID'],$item['UID']);
        if(!$menSeller['ok']){
        $bot->msg("به نظر میرسد اکانت فروشنده در دسترس نمیباشد!! لطفا مجددا جستجو نمایید");
            return ['ok'=>false];
        }
        if(self::isBorrow($ID)){
      $DB->replace("queueBorrow",['IID'=>$mtc[2],"UID"=>$mtc[3],"time"=>time()]);
      $res=  $bot->editMessageReplyMarkup(['chat_id'=>gid,"message_id"=>msgid]);
               $res=  $bot->editMessageReplyMarkup(['chat_id'=>gid,"message_id"=>msgid]);
        $bot->msg("درخواست شما جهت رزرو ایتم فوق ثبت شد و پس از بازگشت از امانت جاری به شما اطلاع داده خواهد شد.");
      
            
        }else{
              $bot->ACQ(cbid,"این ایتم دیگر در دست امانت نیست\nدرخواست امانت به جای رزرو ارسال شد");
            return self::requestToBorrow($ID,$fid);
        }

   
    }
}

class Help2{
    public static $faWord="آابپتثجچحخدذرزژسشصضطظعغفقکگلمنوهی";
    public static   $faNum = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹','آ');
       public static $enNum = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9,'ا');
       public static function findKey($text,$keys){
        foreach($keys as $k=>$v){
            if(is_array($v) && in_array($text,$v))
            return $k;
            elseif(is_string($text) && $text==$v){
                return $k;
            }
        }
       
        return false;
    }
    public static function save($res,$json=true,$name="res.json"){
        file_put_contents($name,$json?json_encode($res,384):$res);
        return $name;
    }
     public static function faToEn($text){
       
return str_replace(self::$faNum,self::$enNum,$text);
         
     }
     
     public static function createKeyboard($ar=[],$count=2){
         
         $keys=[];
         foreach($ar as $k=>$v){
             $keys[]=['text'=>is_array($v)?$v[array_rand($v,1)]:$v];
         }
         
         
    return ['keyboard'=>array_chunk($keys,$count>=1?$count:1),'resize_keyboard'=>true];
    }
    
    public static function scapeChars($text){
        
        return is_null($text)?NULL:str_replace(['*',"_","`","["],['\*','\_','\`','\['],$text);
    }
}
class bot{
public $TOKEN,$Dev,$fileInfo;

public function __construct($token=null){
    $this->TOKEN=is_null($token)?TOKEN:$token;
    global $Dev;
    $this->Dev=$Dev;
    $this->fileInfo=null;


 
   
}

public function cURL($method,$datas=[],$parse_mode='MarkDown',$token=null){
    $token=$token==null?$this->TOKEN:$token;
    if(!isset($datas['parse_mode']))
    $datas['parse_mode']=$parse_mode;
    $url = "https://api.telegram.org/bot".$token."/".$method;
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_POSTFIELDS,$datas);
    $res = curl_exec($ch);
    //file_put_contents('bot.txt',json_encode($res,128|256));
    if(($er=curl_error($ch))==true){
        file_put_contents('bot.txt',json_encode($er,128|256));
        return $er;
    }else{
       // file_put_contents('bot.txt',json_encode($res,128|256));
        return json_decode($res,true);
    }
}
public function leaveChat($cid=null){
    $cid=is_null($cid)?gid:$cid;
    return $this->cURL("leavechat",['chat_id'=>$cid]);
}
public function getCM($cid,$fid){
    $cid=isset($cid)?$cid:gid;
    $fid=isset($fid)?$fid:(rt?rtfid:fid);
    return $this->cURL('getchatmember',['chat_id'=>$cid,'user_id'=>$fid]);
}
public function getFile($fileID){
    return $this->cURL("getfile",["file_id"=>$fileID]);
}
public function mention($fid=null,$cid=null,$limit=10,$pars_mode="MarkDown"){
      $cid=isset($cid)?$cid:gid;
    $fid=isset($fid)?$fid:(rt?rtfid:fid);
   $res= $this->getCM($cid,$fid);
   if(!$res['ok'])
   return $res;
   else{
       $name=$this->scapeChars(mb_substr($res['result']['user']['first_name'],0,$limit));
       return ["ok"=>true,'result'=>['mention'=>preg_match("/html/ius",$pars_mode)?"<a href='tg://user?id=$fid'>".$name."</a>":"[$name](tg://user?id=$fid)"]];
   }
}
public function scapeChars($text){
        
        return is_null($text)?NULL:str_replace(['*',"_","`","["],['\*','\_','\`','\['],$text);
    
}
public function getFURL($path=null,$token=null){
    $token=is_null($token)?$this->TOKEN:$token;
    $path=help::noSet($path)?(is_null($this->fileInfo)?(($res=$this->getFI())!=false?$res['file_path']:false):$this->fileInfo['file_path']):$path;
  //  return $token;
    return $path==false?false:("https://api.telegram.org/file/bot$token/$path");
    
}
public function downloadFile($path=null,$token=null){
    $path=is_array($path)&&isset($path['file_path'])?$path['file_path']:$path;
    $x=$this->getFURL($path,$token);
    return $x==false?false:file_get_contents($x);
}
public function getFI($up=null,$rep=false){
    global $update;
   // if($this->fileInfo!==null)
    //return $this->fileInfo;
    $ret=[];
    $up=is_null($up)?$update:$up;
   
    
     $msg=$up[array_keys($up)[1]];//isset($up['edited_message'])?$up['edited_message']:(isset($up['result'])?$up['result']:$up['message']);
     $msg2=$rep&&isset($msg['reply_to_message'])?$msg['reply_to_message']:$msg;
    $fRes=$this->fLoop($msg2);
   $this->fileInfo=$fRes;
    if(!$rep&&!$fRes&&isset($msg['reply_to_message'])){
        $fRes= $this->fLoop($msg['reply_to_message']);
        $this->fileInfo=$fRes;
        return $fRes;
    }
    return $fRes;
    
    
    
}
private function fLoop($msg){
    $fields=['video','photo','animation','document','audio','voice','sticker'];
      foreach($fields as $fd){
        if(isset($msg[$fd])){
            if($fd=="photo"){
                $img=end($msg[$fd]);
                $file=$this->getFile($img['file_id']);
                $nm=pathinfo($file['result']['file_path'], PATHINFO_EXTENSION);
             $ret=['type'=>$fd,'mime_type'=>"image/$nm",'file_size'=>$img['file_size'],"file_id"=>$img['file_id'],"file_name"=>"TheFTPbot-".rand(0000,9999).".$nm",'file_path'=>$file['result']['file_path']];   
              return $ret;
            }
            else{
              
              //  return ['a'=>"b"];
             $file= $this->getFile($msg[$fd]['file_id']);
              $format=pathinfo($file['result']['file_path'], PATHINFO_EXTENSION);
                $nm=$fd=="audio"?$msg[$fd]['title']."-".$msg[$fd]['performer'].".$format":(isset($msg[$fd]['file_name'])?$msg[$fd]['file_name']:"TheFTPbot-".rand(0000,9999).(isset($msg[$fd]['mime_type'])?".".basename($msg[$fd]['mime_type']):($fd=="sticker"?".$format":"")));
                $mt=isset($msg[$fd]['mime_type'])?$msg[$fd]['mime_type']:help::get_mime_type($nm);
            $ret=['type'=>$fd,'mime_type'=>$mt,'file_size'=>$msg[$fd]['file_size'],"file_id"=>$msg[$fd]['file_id'],"file_name"=>preg_replace("/(\s+)/i","_",$nm),'file_path'=>$file['result']['file_path']];
          // file_put_contents("info.json",json_encode($ret,128|256));
          if(isset($msg[$fd]['duration']))
          $ret['duration']=$msg[$fd]['duration'];
            return $ret;
                
            }
        }
    }
    return false;
}
public function getUS($ch=null,$fid=null){
    $ch=is_null($ch)?gid:$ch;
    $fid=is_null($fid)?(rt?rtfid:fid):$fid;
    $res=$this->getCM($ch,$fid);
    if(!$res['ok'])
    return false;
    return $res['result']['status'];
}
public function isJoin($ch=null,$fid=null){
    $ch=is_null($ch)?gid:$ch;
    $fid=is_null($fid)?(rt?rtfid:fid):$fid;
    return in_array($this->getUS($ch,$fid),['creator','administrator','member']);
}
public function isAdmin($ch=null,$fid=null){
    $ch=is_null($ch)?gid:$ch;
    $fid=is_null($fid)?(rt?rtfid:fid):$fid;
    return in_array($this->getUS($ch,$fid),['creator','administrator']);
}
public function isMember($ch=null,$fid=null){
    $ch=is_null($ch)?gid:$ch;
    $fid=is_null($fid)?(rt?rtfid:fid):$fid;
    return in_array($this->getUS($ch,$fid),['member']);
}
public function getChat($cid){
    return $this-cURL('getchat',['chat_id'=>$cid]);
}
public function getWI($token=null){
   return $this->cURL('getWebhookInfo',null,null,$token);
}
public function delMsg($chatid=null,$msgid=null,$cnt=0){
     $chatid=isset($chatid)?$chatid:gid;

    if(is_null($msgid))
     $msgid=rt ?rtmsgid:msgid-1;
     $result=[];
    $msgs=$cnt==0?[$msgid]: range($msgid,$msgid-$cnt);
    $nm=0;
    $no=0;
    foreach($msgs as $msg){
   $result= $this->cURL('deletemessage',['chat_id'=>$chatid,'message_id'=>$msg]);
       $nm+=$result['ok']?1:0; 
       if(!$result['ok'])
       $no+=1;
       else
       $no=0;
       
       if(($no>=10&&$nm==0) || $no>=20)
       break;
    }
    $res=['ok'=>$nm==0&&$no>=10?false:true,'count'=>$no>=10?0:$nm];
    return $res;
}

public function sd($chatid=null,$file,$cap=null,$name=null){
    $name=is_null($name)?basename($file):$name;
    if($chatid==null)
    $chatid=gid;
    $fid=$file;
    $flag=false;
    if(preg_match("/^%(.*)/iu",$file,$mtc)){
        $flag=true;
$fid=new CURLFile($mtc[1]);}
if($flag)
$fid->setPostFilename($name);
$params=['chat_id'=>$chatid,'document'=>$fid];
if(!is_null($cap))
$params['caption']=$cap;
return $this->cURL('senddocument',$params);
    
}
public function edit($gid=gid,$mid=msgid,$text=text,$pars='MarkDown',$markup=[]){
     $gid=isset($gid)?$gid:gid;
     $mid=isset($mid)?$mid:msgid;
     $text=isset($text)?$text:text;
     $data=['chat_id'=>$gid,'message_id'=>$mid,'text'=>$text,'parse_mode'=>$pars];
     if(is_array($markup) && count($markup)!=0){
       $data['reply_markup']=json_encode($markup);
   }
  return  $this->cURL('editmessagetext',$data);
    
}
public function fwd($chatid=null,$fromchat=null,$msg=null,$noty=false){
    if($chatid==null)
    $chatid=gid;
     if($fromchat==null)
    $fromchat=gid;
     if($msg==null)
    $msg=rt?rtmsgid:msgid-1;
 
return $this->cURL('forwardMessage',['chat_id'=>$chatid,'from_chat_id'=>$fromchat,'message_id'=>$msg,'disable_notification'=>$noty]);
    
}

public function sm($chatid=null,$text="Simple Text",$rp=null,$parse_mode='MarkDown',$markup=[]){


    if($chatid==null)
   $chatid=gid;
   $data=['chat_id'=>$chatid,'text'=>$text,'reply_to_message_id'=>$rp,'parse_mode'=>$parse_mode];
   if($rp==null)
   unset($data['reply_to_message_id']);
   if(is_array($markup) && count($markup)!=0){
       $data['reply_markup']=json_encode($markup);
   }
   return $this->cURL('sendmessage',$data);
    
}

public function __call($m,$args=[]){
    $args=end($args);
    if(!isset($args['chat_id']))
    $args['chat_id']=gid;
   // $this->msg(json_encode($args,128|256));
    return $this->cURL($m,$args);
}
public function msg($text="Simple",$rp=msgid,$parse_mode='MarkDown',$markup=[]){
   
  return $this->sm(gid,$text,$rp,$parse_mode,$markup);
    
}
public function ACQ($id,$text,$show=true){
  return  $this->cURL('answerCallbackQuery',['callback_query_id'=>$id,'text'=>$text,'show_alert'=>$show]);
    
}


}
class help{
    public static function get_mime_type($fname,$rev=false){
    
     $mimet = array( 
        'txt' => 'text/plain',
        'htm' => 'text/html',
        'html' => 'text/html',
        'php' => 'text/html',
        'css' => 'text/css',
        'js' => 'application/javascript',
        'json' => 'application/json',
        'xml' => 'application/xml',
        'swf' => 'application/x-shockwave-flash',
        'flv' => 'video/x-flv',
        'webp' => 'image/webp',
       
        // images
        'png' => 'image/png',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
        'bmp' => 'image/bmp',
        'ico' => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif' => 'image/tiff',
        'svg' => 'image/svg+xml',
        'svgz' => 'image/svg+xml',

        // archives
        'zip' => 'application/zip',
        'rar' => 'application/x-rar-compressed',
        'exe' => 'application/x-msdownload',
        'msi' => 'application/x-msdownload',
        'cab' => 'application/vnd.ms-cab-compressed',

        // audio/video
        'mp3' => 'audio/mpeg',
        'qt' => 'video/quicktime',
        'mov' => 'video/quicktime',

        // adobe
        'pdf' => 'application/pdf',
        'psd' => 'image/vnd.adobe.photoshop',
        'ai' => 'application/postscript',
        'eps' => 'application/postscript',
        'ps' => 'application/postscript',

        // ms office
        'doc' => 'application/msword',
        'rtf' => 'application/rtf',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        'docx' => 'application/msword',
        'xlsx' => 'application/vnd.ms-excel',
        'pptx' => 'application/vnd.ms-powerpoint',


        // open office
        'odt' => 'application/vnd.oasis.opendocument.text',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
      
    );
 $mimet=$rev?array_flip($mimet):$mimet;
 $idx=$rev?$fname:pathinfo($fname, PATHINFO_EXTENSION);
 if(!$rev && (empty($idx) || !isset($idx) || is_null($idx)))
  return 'application/octet-stream';
        return $mimet[$idx];

       
}
public static function noSet($a){
    return is_null($a)||!isset($a)||empty($a)||$a==false;
}
}



