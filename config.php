<?php

require("db.php");
require("class.php");
require("jdf.php");
date_default_timezone_set("Asia/Tehran");

//file_put_contents(json_encode($update,128|256),"1",FILE_APPEND);
define('TOKEN',""); // bot token as string
const DBName=""; // DB-name
const DBHost=""; // DB-host
const DBUser=""; // DB-user
const DBPass=""; // DB-pass
$TMPPATH="tmp";
const maxDPD=50;

const AllowDownloadCount=5;
const limitListShow=30;
const verifyChatID=357377817;
const floodTime=90;
const floodCount=22;
const silentTime=600;
$DB=new DB(DBName.";".DBHost.";".DBUser.";".DBPass);
$update = json_decode(file_get_contents('php://input'),true);
file_put_contents("up.json",json_encode($update,384));
 $bot=new bot(TOKEN);
 $itemGroup=[];
 $startMessage="خوش آمدید";
$Dev=[357377817];
$conf=json_decode(file_get_contents('conf.json'),1);
$mode=$conf['mode']=="ON"?"خاموش":"روشن";
     
 $group=[];//group info
 $user=[];//user info
$lockCount=32;
  if(!isset($update)||isset($update['inline_query']))
  return;
$iscb=isset($update['callback_query']);
$cb=$iscb?$update['callback_query']:$update;
    $msge=array_keys($update);
$msge=$iscb?'message':$msge[1];

 $Edit=$msge=='edited_message'?true:false;
 define('data',$iscb?$cb['data']:null);
define('cbid',$iscb?$cb['id']:0);
     define('gid',$cb[$msge]['chat']['id']);
     define('fid',$iscb?$cb['from']['id']:$cb[$msge]['from']['id']);
     define('caption',isset($cb[$msge]['caption'])?$cb[$msge]['caption']:null);
     define('text',isset($cb[$msge]['text'])?$cb[$msge]['text']:caption);
     define("isPhoto",isset($cb[$msge]['photo']));
// if(!is_null(text) && is_null(caption))
//exit(200);
     define('fname',$iscb?$cb['from']['first_name']:$cb[$msge]['from']['first_name']);
     define('un',$iscb?$cb['from']['username']:$cb[$msge]['from']['username']);
     define('msgid',$cb[$msge]['message_id']);
        
      define('rt',isset($update[$msge]['reply_to_message'])?true:false);
     define('rtmsgid',rt?$update[$msge]['reply_to_message']['message_id']:null);
    define('rtfid',rt?$update[$msge]['reply_to_message']['from']['id']:null);
     define('rttext',rt?(isset($update[$msge]['reply_to_message']['text'])?$update[$msge]['reply_to_message']['text']:$update[$msge]['reply_to_message']['caption']):null);
      define('rtfname',rt?$update[$msge]['reply_to_message']['from']['first_name']:null);

$Mod=[];

$j=['*'];
if(in_array($cb[$msge]['chat']['type'],["group",'supergroup']) && !in_array(gid,$itemGroup))
$bot->leaveChat();
//define('isMode',in_array(fid,$Mod));
define("isDev",in_array(fid,$Dev)?true:false);
$menu=["addItem"=>['افزودن آیتم'],'searchItem'=>['جستجوی آیتم','جستجو'],'showmyitem'=>['ایتم های من','موارد من']];

$isSaleMenu=["inline_keyboard"=>[
    [['text'=>"فروش",'callback_data'=>"/setForSale 1"],['text'=>"امانت",'callback_data'=>"/setForSale 0"]]
    ]];
    $sureResetMenu=['inline_keyboard'=>[
        [['text'=>"100٪ مطمئن هستم، حذف شود.",'callback_data'=>"/resetStep 1"],['text'=>"خیر حذف نشود",'callback_data'=>'/exit']]
        ]];
        $setTypeMenu=['inline_keyboard'=>[
            [['text'=>"کتاب","callback_data"=>"/setType book"],['text'=>"جزوه","callback_data"=>"/setType handout"]]
            ]];
            $cancelMenu=['inline_keyboard'=>[
                    [
                        ['text'=>"انصراف","callback_data"=>"/cancel"]
                        ]
                    ]];
                    
                    $myItemMenu=['inline_keyboard'=>[
                        [['text'=>"ثبت کرده",'callback_data'=>"/submitedItem"],['text'=>"دریافت شده",'callback_data'=>"/recivedItem"]]
                        ]];
                        
                        $adminPanel=["keyboard"=>[
                            [['text'=>"پیام همگانی"],['text'=>"$mode کردن ربات"]]
                            ],'resize_keyboard'=>true];
                            

//9034383