<?php
require_once "config.php";
$user=[];
if($conf['mode']=="OFF" and !isDev){
    $bot->msg("ربات در حال بروزرسانی است\n لطفا بعدا مجددا امتحان کنید");
    exit(http_response_code(200));
}
$silent=$DB->select('silent',['*'],'UID='.fid);
if(!isDev && !empty($silent)){
    $silent=$silent[0];
    if($silent['expire']-time()<=0){
        $DB->delete('silent','UID='.fid);
    }else
    exit(200);
}


    
$ftcUser=$DB->select("user",['*'],"UID=".fid);
if(count($ftcUser)==0){
    $DB->insert("user",['UID'=>fid]);
}else{
    $user=$ftcUser[0];
}
if($user['floodCount']>=floodCount and !isDev){
    $m=silentTime/60;
    $TX="به دلیل درخواست رگباری به مدت $m دقیقه در لیست سکوت قرار گرفتید.";
    $bot->msg($TX);
    if($iscb)
    {
        $bot->ACQ(cbid,"$TX");
    }
    $s=$DB->replace("silent",['UID'=>fid,"expire"=>time()+silentTime]);
 
    
}


 if(!$iscb){  
     if(preg_match("/^\/start/ius",text)){
         if( User::resetStep(fid)){
         $bot->msg($startMessage,msgid,"MarkDown",Help2::createKeyboard($menu));
         }        
     }
     elseif(preg_match("/^\/test\s+(.*)$/ius",text,$mtc)){
         file_put_contents("text.txt",Help2::scapeChars($mtc[1]));
       $bot->sd(null,"%text.txt");//send document
            }
    elseif(User::haveStep()){
        if(isDev)
        {
        if(User::isStep("s2a")){
            $c=["offset"=>0 , "text"=>text,"admin"=>fid];
            $cnf=json_decode(file_get_contents("s2a.json"),1);
            $cnf[]=$c;
            Help2::save($cnf,1,"s2a.json");
            $bot->msg("درخواست شما جهت ارسال پیام همگانی در صف ارسال قرار گرفت.");
            User::setStep(fid,null);
        }    
            
        }
       
        if(User::isStep("addName")){
            
            if(!is_null(text)){
                $ID=$ID=Item::getItemIDByUserStep();
                $DB->update("item",["name"=>Help2::faToEn(text)],"ID=".$ID);
               
                
                $type=Item::selectItem($ID,['type'])['type']==="book"?"کتاب":"جزوه";
                User::setStep(null,"addItem/$ID/addImage");
                $path=Item::createCurrentPath(fid)['result'].PHP_EOL;
                $bot->msg($path."اکنون یک عکس از $type مورد نظر ارسال کنید");
                
            }else{
                $bot->msg("*خطا*\n تنها متن مورد قبول است");
            }
        }
        elseif(User::isStep("addImage")){
            if(isPhoto){
                $ID=$ID=Item::getItemIDByUserStep();
                $DB->update("item",["image"=>$bot->getFI()['file_id']],"ID=".$ID);
               
                  $type=Item::selectItem($ID,['type'])['type']==="book"?"کتاب":"جزوه";
                User::setStep(null,"addItem/$ID/addAbout");
                $path=Item::createCurrentPath(fid)['result'].PHP_EOL;
                $bot->msg($path."اکنون یک متن در باره $type خود ارسال کنید.\nاین متن میتواند شامل نام نویسنده سال چاپ و یا کلمات کلیدی خاصی باشد که بقیه کاربران با آنها بتوانند $type شما را راحت تر جستجو کنند.");
                
            }else{
                 $bot->msg("*خطا*\n تنها عکس مورد قبول است");
            }
        }
        elseif(User::isStep("addAbout")){
            if(!is_null(text)){
                
                $ID=$ID=Item::getItemIDByUserStep();
                $DB->update("item",["about"=>Help2::faToEn(text)],"ID=".$ID);
              
                $item=Item::selectItem($ID,['type','forSale']);
                $type=$item['type'];
                  $type2=$type==="book"?"کتاب":"جزوه";
                  $spc=$item['forSale']==="1"?"قیمت $type2 خود را به ریال وارد کنید":"حد اکثر تعداد روز مورد انتظار برای برگرداندن $type2 را به عدد وارد کنید";
                User::setStep(fid,"addItem/$ID/addSpecialField");
                $path=str_replace(["*",":"],"",Item::createCurrentPath(fid)['result'])." > *".($item['forSale']==="1"?"قیمت":"مدت")."*".PHP_EOL;
                
                $bot->msg($path."اکنون $spc");
                
            }else{
                 $bot->msg("*خطا*\n تنها متن مورد قبول است");
            }
        }
         elseif(User::isStep("addSpecialField")){
            if(!is_null(text)){
                $text=Help2::faToEn(text);
                $ID=$ID=Item::getItemIDByUserStep();
                $item=Item::selectItem($ID,['type','forSale']);
                $type=$item['type'];
                  $type=$type==="book"?"کتاب":"جزوه";
                if($item['forSale']==="1"){ // for sale
                    if(is_numeric($text) && $text>=0){
                        $DB->insert('forSale',['ID'=>$ID,'price'=>$text]);
                       
                        $res=$bot->msg("*درخواست شما در صف تایید مدیریت قرار گرفته است و پس از تایید افراد میتوانند با جستجو در ربات $type شما را جستجو کنند*");
                        $DB->update('item',['addTime'=>time(),'msgid'=>$res['result']['message_id']],'ID='.$ID);
                        User::resetStep(null,true);
                       $res= Item::sendItemToVerify($ID);
                    
                       
                       
                    }
                    else{
                        $bot->msg("*خطا*\n تنها صفر و اعداد مثبت مورد تایید است.");
                    }
                }
                else{ //for borrow
                     if(is_numeric($text) && $text>=1){
                        $DB->insert('forBorrow',['ID'=>$ID,'maxTimeOfBack'=>$text]);
                        
                      $res=  $bot->msg("*درخواست شما در صف تایید مدیریت قرار گرفته است و پس از تایید افراد میتوانند با جستجو در ربات $type شما را جستجو کنند*");
                        $DB->update('item',['addTime'=>time(),'msgid'=>$res['result']['message_id']],'ID='.$ID);
                        User::resetStep(null,true);
                        $res= Item::sendItemToVerify($ID);
                     
                        
                    }
                    else{
                        $bot->msg("*خطا*\n تنها یک و اعداد بزرگ تر از آن مورد تایید است.");
                    }
                    
                }
                
                
                
                
                 
                
            }else{
                 $bot->msg("*خطا*\n تنها عدد مورد قبول است");
            }
        }
        elseif(User::isStep('searchItem')){
           
            $items=Item::searchItemByQuery(text);
            if(count($items)==0){
                $bot->msg("هیچ موردی یافت نشد!!!",msgid,"MarkDown",$cancelMenu);
            }else{
			$bot->msg('نتایج برای جستجوی شما:',msgid,"MarkDown",["inline_keyboard"=>Item::createItemKeyboard($items )]);}
			//	User::resetStep(null,1);
		}
		     elseif(User::isStep('getTimeOfBack')){
                   
                   $text=Help2::faToEn(text);
                   if(is_numeric($text)){
                       $ID=explode("/",$user['step'])[1];
                       $max=Item::getMaxTimeOfBack($ID);
                       if($max==-1){
                           $bot->msg("خطا!!\n*امکان اامانت این ایتم وجود ندارد*");
}
                        elseif($text<=0 or $text>$max){
                               
                           $bot->msg("حداکثر روز مورد مجاز `$max` روز و حداقل آن `1` روزمیباشد");
                       }
                       else{
                         Item::FinalRequestToBorrow($ID,$text);  
                         User::setStep(fid,null);
                       }
                   }else{
                       $bot->msg("خطا!!\n تنها عدد مورد تایید است.");                   }
               }
      /*  elseif(!is_null(text)){
             $key=Help2::findKey(text,$menu);
             if(User::isStep("addItem")){
            
             }
        }*/
        
    }else{
        if(!is_null(text)){
             $key=Help2::findKey(text,$menu);
             if(preg_match('/^addItem$/ius',$key)){
                 $bot->msg("در حال انتقال به منوی افزودن آیتم جدید...",msgid,"MarkDown",['remove_keyboard'=>true]);
                 usleep(800);
               $res=  $bot->msg("قصد فروش و یا امانت را دارید؟",msgid+1,"MarkDown",$isSaleMenu);
               
                 User::setStep(fid,"addItem");
             }
             elseif(preg_match('/^searchItem$/ius',$key)){
               
              
				 $bot->msg("نام کتاب، جزوه ویا نویسنده مورد نظر خود را ارسال کنید.
				 در برخی موارد ب اوردن تاریخ چاپ  و یا جزئیات دیگر نیز میتوانید در جزئیات دیگر ایتم نیز جستجو کنید به عنوان مثال 'داستان' آیتم هایی که در جزئیات کتاب کلمه داستان ذکر شده است را لیست میکند",msgid,"MarkDown",$cancelMenu);
User::setStep(fid,"searchItem");                
		
                 
             }
              elseif(preg_match('/^showmyitem$/ius',$key)){
           $bot->msg("یکی از موارد زیر را انتخاب کنید",msgid,"MarkDown",$myItemMenu);
                
               
                
              }
              if(isDev){
                  if(preg_match("/^[\/!](panel|پنل)/ius",text))
                  {
                      
                      $bot->msg("به پنل مدیریت خوش آمدید",msgid,"MarkDown",$adminPanel);
                  }
                  elseif(preg_match("/^پیام همگانی/ius",text))
                  {
                     $bot->msg("لطفا پیام خود را وارد کنید ( فقط متن)"); 
                     User::setStep(fid,"s2a");
                  }
                  elseif(preg_match("/^(روشن|خاموش)\sکردن\sربات$/ius",text,$mtc))
                  {
                        $newMode=$mtc[1]=="روشن"?"ON":"OFF";
                        $conf['mode']=$newMode;
                        $mode=$mtc[1]=="روشن"?"خاموش":"روشن";
                          $adminPanel=["keyboard"=>[
                            [['text'=>"پیام همگانی"],['text'=>"$mode کردن ربات"]]
                            ],'resize_keyboard'=>true];
                        Help2::save($conf,1,"conf.json");
                        $bot->msg("ربات {$mtc[1]} شد.",msgid,"MarkDown",$adminPanel);
                  }
              }
        }
        
    }
    }
    else{
       
        if(isDev){
            
            if(preg_match("/^\/verify\s+(\d+)$/ius",data,$mtc)){
               
                $item=Item::selectItem($mtc[1],['UID','ID','addTime','msgid']);
                $date=jdate("Y/m/d h:i:s" ,$item['addTime'], null ,"Asia/Tehran" ,"en" );
                $bot->sm($item['UID'],"درخواست شما با آیدی {$mtc[1]} که در $date ثبت کرده بودید توسط مدیر *تایید* شد.",$item['msgid']);//for replay to request message to unconfused customer
            $DB->update('item',['active'=>1],"ID=".$mtc[1]);
          $res=  $bot->editMessageReplyMarkup(['chat_id'=>gid,"message_id"=>msgid]);
       
                
            }
            elseif(preg_match("/^\/disapproval\s+(\d+)$/ius",data,$mtc)){
                
               $item=Item::selectItem($mtc[1],['UID','ID','addTime','msgid','forSale']);
                $date=jdate("Y/m/d h:i:s" ,$item['addTime'], null ,"Asia/Tehran" ,"en" );
                $bot->sm($item['UID'],"درخواست شما با آیدی {$mtc[1]} که در $date ثبت کرده بودید توسط مدیر *رد* شد.",$item['msgid']);
            $DB->delete('item',"ID=".$mtc[1]);
            $DB->delete($item['forSale']==="1"?"forSale":"forBorrow","ID=".$mtc[1]);
          $res=  $bot->editMessageReplyMarkup(['chat_id'=>gid,"message_id"=>msgid]);
          //goto test .. send me req
                
            }
        }
        if(preg_match("/^\/exit\s*(.*)$/ius",data,$mtc)){
            $bot->delMsg(gid,msgid);
            User::setStep(fid,null);
        }
        elseif(preg_match("/^\/cancel\s*(.*)$/ius",data,$mtc)){
            
            User::setStep(fid,null);
            $bot->editMessageReplyMarkup(['chat_id'=>gid,"message_id"=>msgid]);
            $bot->msg($startMessage);
         
        }
        elseif(preg_match("/^\/submitedItem$/ius",data)){
            $bot->editMessageReplyMarkup(['chat_id'=>gid,"message_id"=>msgid]);
               $items=Item::selectUserItems(fid);
                if(count($items)==0){
                    $bot->msg("شما هیچ ایتمی ثبت نکرده اید");
                }
                else{
                   $bot->msg("ایتم های ثبت شده ی شما:",msgid,"MarkDown",['inline_keyboard'=>Item::createUserItemKeyboard ( $items)]);
                }
        }
         elseif(preg_match("/^\/recivedItem$/ius",data)){
            $bot->editMessageReplyMarkup(['chat_id'=>gid,"message_id"=>msgid]);
               $items=Item::selectUserRecivedItems(fid);
                if(count($items)==0){
                    $bot->msg("شما هیچ ایتمی  دریافت نکرده اید");
                }
                else{
                   Help2::save($bot->msg("ایتم های ثبت شده ی شما:",msgid,"MarkDown",['inline_keyboard'=>Item::createUserReciveditemKeyboard ( $items)]));
                }
        }
        elseif(preg_match("/^\/(accept|disapproval)BuyRequest\s+(\d+)\s+(\d+)$/ius",data,$mtc)){
            if(preg_match("/accept/ius",$mtc[1])){
                $res=  $bot->editMessageReplyMarkup(['chat_id'=>gid,"message_id"=>msgid]);
                $bot->ACQ(cbid,"درخواست تایید شد");
                $bot->sm($mtc[3],"درخواست خرید ی که به ".$bot->mention(fid)['result']['mention']." فرستاده بودید *تایید* شد.");
                $DB->insert("sell",['IID'=>$mtc[2],"UID"=>$mtc[3],"time"=>time()]);
                
            }else{
                $res=  $bot->editMessageReplyMarkup(['chat_id'=>gid,"message_id"=>msgid]);
                $bot->ACQ(cbid,"درخواست رد شد.");
                $bot->sm($mtc[3],"درخواست خرید ی که به ".$bot->mention(fid)['result']['mention']." فرستاده بودید *رد* شد.");
            }
        }
         elseif(preg_match("/^\/(accept|disapproval)BorrowRequest\s+(\d+)\s+(\d+)\s+(\d+)$/ius",data,$mtc)){
             $res=  $bot->editMessageReplyMarkup(['chat_id'=>gid,"message_id"=>msgid]);
            if(preg_match("/accept/ius",$mtc[1])){
                
                $bot->ACQ(cbid,"درخواست تایید شد");
                $bot->sm($mtc[4],"درخواست امانتی که به ".$bot->mention(fid)['result']['mention']." فرستاده بودید *تایید* شد.");
                $DB->replace("borrow",['IID'=>$mtc[2],'duration'=>$mtc[3],"UID"=>$mtc[4],"time"=>time()]);
                
            }else{
                
                $bot->ACQ(cbid,"درخواست رد شد.");
                $bot->sm($mtc[4],"درخواست امانتی که به ".$bot->mention(fid)['result']['mention']." فرستاده بودید *رد* شد.");
            }
        }
          elseif(preg_match("/^\/myshow\s+(\d+)$/ius",data,$mtc)){
                 $ID=$mtc[1];
                 
                 Item::sendUserItem($ID);
                
             }
              elseif(preg_match("/^\/myshowrecived\s+(\d+)$/ius",data,$mtc)){
                 $ID=$mtc[1];
                 
                 Item::sendUserRecivedItem($ID);
                
             }
             elseif(preg_match("/^\/backBeforeDue\s+(\d+)\s+(\d+)$/ius",data,$mtc)){
       
                 $DB->update("borrow",['duration'=>0]," IID={$mtc[2]} and UID={$mtc[1]} limit 1");
                  $bot->editMessageReplyMarkup(['chat_id'=>gid,"message_id"=>msgid]);
                  $bot->ACQ(cbid,"تحویل پیش از موعد اعمال شد.");
             }
           elseif(User::haveStep()){
           
               if(preg_match("/^\/resetStep\s+(\d+)$/ius",data,$mtc)){
                   if($mtc[1]==1){
                       $ID=Item::getItemIDByUserStep();
                       $DB->delete("item","ID=".$ID);
                       $bot->delMsg(gid,msgid);
                       User::resetStep(fid,1);
                       $bot->msg("reseted");
                       $bot->msg($startMessage,-1,"MarkDown",Help2::createKeyboard($menu));
                   }
               }
          
            elseif(User::isStep('addItem')){
                if(preg_match("/^\/setForSale\s+(\d+)$/ius",data,$mtc)){
                   $ID= Item::addItem($mtc[1]);
                   $fs=$mtc[1]==1?"فروش":"امانت";
                   
                  
                 // $bot->delMsg(gid,msgid+1);
                 $path=Item::createCurrentPath(fid)['result'].PHP_EOL;
                  $res= $bot->msg("افزودن ایتم > *نوع*:\n شما در حال افزودن یک آیتم برای *$fs* هستید.\nاکنون مشخص کنید قصد $fs کدام یک را دارید",msgid,"MarkDown",$setTypeMenu);
                   $bot->delMsg(gid,msgid);
                  //file_put_contents("res.json",json_encode($res,384));
                }
                elseif(preg_match("/^\/setType\s+(\w+)$/ius",data,$mtc)){
                    $type=$mtc[1]==="book"?"کتاب":"جزوه";
                   
                    $ID=Item::getItemIDByUserStep();
                    if($mtc[1]!=="book")
                        $DB->update('item',['type'=>$mtc[1]],'ID='.$ID);
                    User::setStep(null,"addName",true);
                    $path=Item::createCurrentPath(fid)['result'].PHP_EOL;
                    $bot->msg($path."اکنون نام *$type* را وارد کنید:\n به عنوان مثال:`تاریخ تحلیلی صدر  اسلام`");
                     $bot->delMsg(gid,msgid);
                }
                
        }
         elseif(User::isStep('searchItem')){
            
             if(preg_match("/^\/show\s+(\d+)$/ius",data,$mtc)){
                 $ID=$mtc[1];
                 Item::sendSearchedItem($ID);
                
             }
            
             elseif(preg_match("/^\/buy\s+(\d+)$/ius",data,$mtc)){
                 Item::requestToBuy($mtc[1]);
                 
             }
             elseif(preg_match("/^\/borrow\s+(\d+)$/ius",data,$mtc)){
                 Item::requestToBorrow($mtc[1]);
                 
             }
             elseif(preg_match("/^\/reserve\s+(\d+)$/ius",data,$mtc)){
                 Item::requestToReserve($mtc[1]);
                 
             }
             
         }
         
        
    }
        
    }
    
    
    $flood=time()-$user['floodTime']>floodTime||$user['floodTime']==0?" floodTime=".time()." , floodCount=1 ":" floodCount=floodCount+1 ";
 
    $DB->pdo()->prepare("update user set $flood where UID=?")->execute([fid]);